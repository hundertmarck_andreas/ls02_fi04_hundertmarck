/**
 * Ladung dient dem Umgang der jeweiligen Ladungen, Inhalte und Menge dieser fuer die Raumschiffe.
 * @author Hundertmarck 
 * @version 1.1
 */

public class Ladung {
	
	//Attribute:
	/**
	 * Name des Inhalts einer Ladung 
	 */
	private String inhalt;
	/**
	 * Menge eines Inhalts einer Ladung 
	 */
	private int menge;
	
	//Konstruktoren:
	/**
     * Standard Konstruktor fuer die Klasse Ladung
     * @param inhalt Meldung bei keiner Angabe fuer Bezeichnung der Ladung
     * @param menge Anzahl der Ladung bei keiner Angabe
     */
	public Ladung() {
		this.inhalt = "Kein Ladungsinhalt an Bord";
		this.menge = 0;
	}
	
	/**
     * Konstruktor fuer die Klasse Ladung
     * @param inhalt der Name fuer das Ladungselement
     * @param menge die Anzahl des Ladungselements
     */
	public Ladung(String inhalt, int menge)
	{
	  setInhalt(inhalt);
	  setMenge(menge);
	}
	
	//Getter/Setter:

	/**
	* @return Aktuell verwendeter Inhalt aus dem Ladungsverzeichnis
	*/
	public String getInhalt() {
		return inhalt;
	}

	/**
	* @param inhalt Inhalt einer Ladung die bestimmt wird (Bezeichnung des Objekts)
	*/
	public void setInhalt(String inhalt) {
		this.inhalt = inhalt;
	}

	/**
	* @return Aktuell verwendete Menge einer Ladung (Anzahl in St�ck)
	*/
	public int getMenge() {
		return menge;
	}

	/**
	* @param menge Menge einer Ladung die bestimmt wird (Anzahl in St�ck)
	*/
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	//
	

}
