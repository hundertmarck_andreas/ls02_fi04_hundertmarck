import java.util.ArrayList;

/**
 * Krieg der Raumschiffe beschreibt den Kampf zwischen den verschiedenen Intergalaktischen Voelkern und ihrer erbitterter Konfrotation zwischen den Sternen!
 * @author Hundertmarck 
 * @version 1.1
 */

public class Main_Krieg_der_Raumschiffe {

	/**
	* Erstellung der einzelnen Schiffe, ihrer Ladungen und Ablauf des Geschehens in der Main Methode.
	* @param klingonen ist das Schiff der Klingonen unter dem Namen "IKS Hegh'ta"
	* @param romulaner ist das Schiff der Romulaner unter dem Namen "IRW Khazara"
	* @param vulkanier ist das Schiff der Vulkanier unter dem Namen "Ni'var"
	* @param ferengiS ist die Ladung "Ferengi Schneckensaft"
	* @param borgS ist die Ladung "Borg-Schrott"
	* @param roteM ist die Ladung "Rote Materie"
	* @param forschungsS ist die Ladung "Forschungssonde"
	* @param batKS ist die Ladung "Bat'leth Klingonen Schwert"
	* @param plasmaW ist die Ladung "Plasma-Waffe"
	* @param photonenT ist die Ladung "Photonentorpedos" 
	*/
	public static void main(String[] args) {
		
		//Raumschiffe initialisieren:
		/*
		 * public Raumschiff(String schiffsname, int energieversorgung, int schutzschilde, 
		 * int lebenserhaltungssysteme, int huelle, int reparaturAndroiden, 
		 * int photonentorpedo, ArrayList<Ladung> ladungsVerzeichnis)
		 * 
		 */
		
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 2, 1, new ArrayList<Ladung>());
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2, new ArrayList<Ladung>());
		Raumschiff vulkanier = new Raumschiff("Ni'var", 80, 80, 100, 50, 5, 0, new ArrayList<Ladung>());

		//Ladungen initialisieren:
		
		Ladung ferengiS = new Ladung("Ferengi Schneckensaft", 200);
		Ladung borgS = new Ladung("Borg-Schrott", 5);
		Ladung roteM = new Ladung("Rote Materie", 2);
		Ladung forschungsS = new Ladung("Forschungssonde", 35);
		Ladung batKS = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung plasmaW = new Ladung("Plasma-Waffe", 50);
		Ladung photonenT = new Ladung("Photonentorpedos", 3);
		
		//Schiffe beladen:
		
		klingonen.schiffBeladung(ferengiS);
		klingonen.schiffBeladung(batKS);
		romulaner.schiffBeladung(borgS);
		romulaner.schiffBeladung(roteM);
		romulaner.schiffBeladung(plasmaW);
		vulkanier.schiffBeladung(forschungsS);
		vulkanier.schiffBeladung(photonenT);
		
		//Ablauf:
		//Runde 1)
		//Klingonen feuern einen Photonentorpedo auf das Schiff der Romulaner ab.
		klingonen.schiessPhotonentorpedo(romulaner);
		
		//Runde 2)
		//Romulaner schie�en mit ihrer Phaserkanone auf das Schiff der Klingonen zur�ck.
		romulaner.schiessPhaserkanone(klingonen);
		
		//Runde 3)
		//Vulkanier senden eine Nachricht an alle.
		vulkanier.sendeNachricht("Gewalt ist nicht logisch");
		
		//Runde 4)
		//Klingonen schauen den Zustand ihres Schiffes nach und geben sich ihr Ladungsverzeichnis aus.
		klingonen.gebeZustandAus();
		klingonen.gebeLadungsverzeichnisAus();
		
		//Runde 5)
		//Vulkanier senden alle ihre Reparatur Androiden aus.
		vulkanier.sendeReparaturauftrag(true, true, true, vulkanier.getReparaturAndroiden());
		
		//Runde 6)
		//Vulkanier laden alle Photonentorpedos und r�umen im Anschluss ihr Ladungsverzeichnis auf.
		vulkanier.ladePhotonentorpedos(3);
		vulkanier.raeumeLadungsverzeichnisAuf();
		
		//Runde 7)
		//Klingonen versuchen erneut auf das Schiff der Romulaner zu schie�en und wiederholen diesen Vorgang.
		klingonen.schiessPhotonentorpedo(romulaner);
		klingonen.schiessPhotonentorpedo(romulaner);
		
		//Runde 8)
		//Klingonen, Romulaner und Vulkanier lassen sich den Zustand und das Ladungsverzeichniss ihres Schiffes ausgeben.
		klingonen.gebeZustandAus();
		klingonen.gebeLadungsverzeichnisAus();
		romulaner.gebeZustandAus();
		romulaner.gebeLadungsverzeichnisAus();
		vulkanier.gebeZustandAus();
		vulkanier.gebeLadungsverzeichnisAus();
		
	}

}
