import java.util.ArrayList;
import java.util.Random;

/**
 * Raumschiff beinhaltet alle Methoden im Umgang mit den Raumschiffen.
 * @author Hundertmarck 
 * @version 1.1
 */

public class Raumschiff {
	
	//Attribute
	//
	/**
	 * Name des Schiffes 
	 */
	private String schiffsname;
	/**
	 * Energieversorgung eines Schiffes 
	 */
	private int energieversorgung;
	/**
	 * Schutzschilde eines Schiffes 
	 */
	private int schutzschilde;
	/**
	 * Lebenserhaltungssysteme eines Schiffes 
	 */
	private int lebenserhaltungssysteme;
	/**
	 * Huelle eine Schiffes 
	 */
	private int huelle;
	/**
	 * Anzahl einsatzbereiter Reparatur Androiden eines Schiffes 
	 */
	private int reparaturAndroiden;
	/**
	 * Anzahl geladener Photonentorpedos eines Schiffes 
	 */
	private int photonentorpedo;
	/**
	 * Broadcast Kommunikator als Mittel der Kommunikation zwischen allen Schiffen 
	 */
	private static ArrayList<String> broadcastKommunikator = new ArrayList<>();
	/**
	 * Das Ladungsverzeichnis eines Schiffes 
	 */
	private ArrayList<Ladung> ladungsVerzeichnis;
	
	//Konstruktoren
	//
	/**
     * Standard Konstruktor fuer die Klasse Raumschiff
     * @param schiffsname Bezeichnung eines Schiffes ohne Namensangabe
     * @param energieversorgung Wert der Energieversorgung bei keiner Festlegung
     * @param schutzschilde Wert der Schutzschilde bei keiner Festlegung
     * @param lebenserhaltungssysteme Wert der Lebenserhaltungssysteme bei keiner Festlegung
     * @param huelle Wert der Schiffshuelle bei keiner Festlegung
     * @param reparaturAndroiden Anzahl der vorhandenen Reparatur Androiden wenn keine angegeben
     * @param photonentorpedo Anzahl der Photonentorpedos wenn keine angegeben
     */
	public Raumschiff() {
		this.schiffsname = "Unknown";
		this.energieversorgung = 0;
		this.schutzschilde = 0;
		this.lebenserhaltungssysteme = 0;
		this.huelle = 0;
		this.reparaturAndroiden = 0;
		this.photonentorpedo = 0;
	}
	
	/**
     * Konstruktor fuer die Klasse Raumschiff
     * @param schiffsname Bezeichnung des Raumschiffes (zur Kennung)
     * @param energieversorgung Zustand der Energieversorgung des Raumschiffes (in Prozent)
     * @param schutzschilde Zustand der Schutzschilde des Raumschiffes (in Prozent)
     * @param lebenserhaltungssysteme Zustand der Lebenserhaltungssysteme des Raumschiffes (in Prozent)
     * @param huelle Zustand der Schiffshuelle des Raumschiffes (in Prozent)
     * @param reparaturAndroiden Anzahl der einsatzbereiten Reparatur Androiden des Raumschiffes (Anzahl in St�ck)
     * @param photonentorpedo Anzahl der geladenen Photonentorpedos des Raumschiffes (Anzahl in St�ck)
     * @param ladungsverzeichnis Initialisieren des aktuellen Ladungsverzeichnis des Raumschiffes 
     */
	public Raumschiff(String schiffsname, int energieversorgung, int schutzschilde, int lebenserhaltungssysteme, int huelle, int reparaturAndroiden, int photonentorpedo, ArrayList<Ladung> ladungsVerzeichnis)
	{
	  this.schiffsname = schiffsname;
	  this.energieversorgung = energieversorgung;
	  this.schutzschilde = schutzschilde;
	  this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	  this.huelle = huelle;
	  this.reparaturAndroiden = reparaturAndroiden;
	  this.photonentorpedo = photonentorpedo;
	  this.ladungsVerzeichnis = new ArrayList<Ladung>();
	}
	
	//Getter/Setter 
	//
	
	/**
	* @return Aktuell verwendeter Name des Raumschiffs (Zur deutlichen Erkennung)
	*/
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	* @param schiffsname Bestimmen des Namens eines Schiffes
	*/
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	/**
	* @return Aktuelle Angabe der Energieversorgung eines Raumschiffes (in Prozent)
	*/
	public int getEnergieversorgung() {
		return energieversorgung;
	}

	/**
	* @param energieversorgung Bestimmen der Energieversorgung eines Schiffes (in Prozent)
	*/
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	/**
	* @return Aktuelle Angabe der Schutzschilde eines Raumschiffes (in Prozent)
	*/
	public int getSchutzschilde() {
		return schutzschilde;
	}

	/**
	* @param schutzschilde Bestimmen der Schutzschilde eines Schiffes (in Prozent)
	*/
	public void setSchutzschilde(int schutzschilde) {
		this.schutzschilde = schutzschilde;
	}

	/**
	* @return Aktuelle Angabe der Lebenserhaltungssysteme eines Raumschiffes (in Prozent)
	*/
	public int getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}

	/**
	* @param lebenserhaltungssysteme Bestimmen der Lebenserhaltungssysteme eines Schiffes (in Prozent)
	*/
	public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}

	/**
	* @return Aktuelle Angabe der H�lle eines Raumschiffes (in Prozent)
	*/
	public int getHuelle() {
		return huelle;
	}

	/**
	* @param huelle Bestimmen der Huelle eines Schiffes (in Prozent)
	*/
	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}

	/**
	* @return Aktuell zu verwendende Reparatur Androiden eines Raumschiffes (Anzahl in St�ck)
	*/
	public int getReparaturAndroiden() {
		return reparaturAndroiden;
	}

	/**
	* @param reparaturAndroiden Bestimmen der Reparatur Androiden eines Schiffes (Anzahl in St�ck)
	*/
	public void setReparaturAndroiden(int reparaturAndroiden) {
		this.reparaturAndroiden = reparaturAndroiden;
	}

	/**
	* @return Aktuell zu verwendende Photonentorpedos eines Raumschiffes (Anzahl in St�ck)
	*/
	public int getPhotonentorpedo() {
		return photonentorpedo;
	}

	/**
	* @param photonentorpedo Bestimmen der Photonentorpedos eines Schiffes (Anzahl in St�ck)
	*/
	public void setPhotonentorpedo(int photonentorpedo) {
		this.photonentorpedo = photonentorpedo;
	}

	/**
	* @return Aktueller Stand des Broadcast Kommunikators (Nachrichtenvermittler aller Raumschiffe)
	*/
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	/**
	* @param broadcastKommunikator Bestimmen des Broadcast Kommunikators fuer ein Schiff
	*/
	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	/**
	* @return Aktuelle Angabe einer Ladung aus dem Ladungsverzeichnis eines Raumschiffes (Inhalte mit Bezeichnung und Menge)
	*/
	public ArrayList<Ladung> getLadungsVerzeichnis() {
		return ladungsVerzeichnis;
	}

	/**
	* @param energieversorgung Bestimmen des Ladungsverzeichnisses fuer ein Schiff
	*/
	public void setLadungsVerzeichnis(ArrayList<Ladung> ladungsVerzeichnis) {
		this.ladungsVerzeichnis = ladungsVerzeichnis;
	}
	
	
	//Methoden:
	
	//1.) Zustand des Raumschiffs:
	/**
	 * Ausgabe aller Werte zum Zustand des Raumschiffes auf der Konsole 
	 */
	public void gebeZustandAus() {
		System.out.println(this.schiffsname + " Zustand:");
        System.out.println("Energieversorgung: " + this.energieversorgung + " %");
        System.out.println("Schilde: " + this.schutzschilde + " %");
        System.out.println("H�lle: " + this.huelle + " %");
        System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssysteme + " %");
        System.out.println("Anzahl Photonentorpedos: " + this.photonentorpedo);
        System.out.println("Anzahl Androiden: " + this.reparaturAndroiden);
        System.out.println("");
	}
	
	//2.) Ladungsverzeichnis ausgeben:
	/**
	 * Ausgabe aller Ladungen aus dem Ladungsverzeichnis des Raumschiffes auf der Konsole 
	 * Falls das Ladungsverzeichnis leer ist, soll dies angemerkt werden
	 */
	public void gebeLadungsverzeichnisAus() {
		if(this.ladungsVerzeichnis.size() <= 0) {
			System.out.println("Das Schiff ist unbeladen");
	        System.out.println("");
		}
		else {
			for(int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
			System.out.println(this.ladungsVerzeichnis.get(i).getInhalt() + ": " + this.ladungsVerzeichnis.get(i).getMenge());
			}
			System.out.println("");
		}
	}
	
	//Treffer-Methode:
	/**
	 * Ausgabe auf der Konsole, wenn ein Raumschiff getroffen wurde 
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen!");
        System.out.println("");
	}
	
	//3.) Photonentorpedos abschie�en:
	
	/**
	 * Methode zum Abschuss eines Photonentorpedos, wenn die Kanone geladen ist mit Ausgabe auf der Konsole
	 * Falls die Photonenkanone nicht geladen ist, soll dies angemerkt auf der Konsole ausgegeben werden 
	 */
	public void schiessPhotonentorpedo(Raumschiff r) {
		if(this.photonentorpedo >= 1) {
			System.out.println("Photonentorpedos abgeschossen");
			this.photonentorpedo -= 1;
 
			treffer(r);
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	
	//4.) Phaserkanone abschie�en:
	
	/**
	 * Methode zum Abschuss der Phaserkanone, wenn die Energieversorgung des Raumschiffes dafuer reicht und Ausgabe auf der Konsole
	 * Falls die Energieversorgung nicht fuer die Phaserkanone reicht, soll dies angemerkt auf der Konsole ausgegeben werden 
	 */
	public void schiessPhaserkanone(Raumschiff r) {
		if(this.energieversorgung > 50) {
			System.out.println("Phaserkanone abgeschossen");
 
			treffer(r);
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	
	//5.) Logbuch Eintr�ge zur�ckgeben:
	/**
	 * Ausgabe aller Nachrichten der einzelnen Raumschiffe auf der Konsole 
	 */
	public ArrayList<String> logBuch() {
		return Raumschiff.broadcastKommunikator;
	}
	
	//6.) Treffer vermerken:
	
	/**
	 * Bei einem Treffer, verliert das getroffene Schiff 50 Prozent seiner Schutzschilde
	 * Falls die Schilde auf Null fallen, nehmen Schiffshuelle und Energieversorgung 50 Prozent Schaden
	 * Wenn die Schiffshuelle auf Null faellt, wird auf der Konsole ausgegeben, dass seine Lebenserhaltungssysteme vernichtet wurden
	 */
	public void zaehleTreffer() {
		this.schutzschilde -= 50;
 
		if(this.schutzschilde <= 0) {
			this.huelle -= 50;
			this.energieversorgung -= 50;
		}
 
		if(this.huelle <= 0) {
			System.out.println("Die Lebenenserhaltungssysteme wurden vernichtet");
	        System.out.println("");
		}
	}
	
	//7.) Nachricht an Alle:
	/**
	 * Nachricht des aktuellen Raumschiffes und Ausgabe auf der Konsole 
	 */
	public void sendeNachricht(String message) {
		Raumschiff.broadcastKommunikator.add(message);
		System.out.println(this.schiffsname + ": " + message);
        System.out.println("");
	}
	
	//Schiffe beladen:
	/**
	 * Methode zum hinzufuegen einer neuen Ladung in das aktuelle Ladungsverzeichnis eines Schiffes 
	 */
	public void schiffBeladung(Ladung neueLadung) {
		this.ladungsVerzeichnis.add(neueLadung);
	}
	
	//8.) Reparatur Androiden einsetzen:
	/**
	 * Methode fuer den Reparatur Auftrag an Reparatur Androiden des Schiffes
	 * Addiere jedes Mal 1 zu strukturen, wenn die Werte zu Schutzschilde, Energieversorgung oder Schiffshuelle auf True gesetzt sind
	 * Wenn mehr Reparatur Androiden angegeben wurden, als auf dem Schiff vorhanden, benutze die Maximal verfuegbare Anzahl
	 * Berechnung zur finalen Reparatur in Prozent: Zufallswert multipliziert mit der Anzahl der eingesetzten Androiden, geteilt durch Anzahl der beschaedigten Strukturen
	 * Ausgabe des finalen reparierten Wertes des Schiffes auf der Konsole
	 * @param strukturen Gibt die Anzahl der beschaedigten Elemente des Schiffes an
	 * @param r Ein zufaelliger Wert
	 * @param value Ein zufaelliger Wert zwischen 1-100
	 * @param shipStructures Der finale Wert um den das Schiff repariert wurde
	 */
	public void sendeReparaturauftrag(boolean schutzschilde, boolean energieversorgung, boolean huelle, int reparaturAndroiden) {
		int strukturen = 0;
		Random r = new Random();
 
		boolean schutzSchild = r.nextBoolean();
		if(schutzSchild == true) {
			strukturen += 1;
		}
 
		boolean energieVersorgung = r.nextBoolean();
		if(energieVersorgung == true) {
			strukturen += 1;
		}
 
		boolean schiffsHuelle = r.nextBoolean();
		if(schiffsHuelle == true) {
			strukturen += 1;
		}
 
		int value = r.nextInt(101);
 
		if(reparaturAndroiden > this.reparaturAndroiden) {
			this.reparaturAndroiden = reparaturAndroiden;
		}
 
		double shipStructures = ((value * (reparaturAndroiden =- reparaturAndroiden)) / (strukturen =- strukturen));
		System.out.println(this.schiffsname + " wurde um: " + shipStructures + " repariert.");
        System.out.println("");
	}
	
	//9.) Ladung "Photonentorpedos" einsetzen:
	/**
	 * Methode fuer das Laden der Photonentorpedos
	 * Ausgabe auf der Konsole, wenn keine Photonentorpedos vorhanden sind
	 * Wenn mehr Photonentorpedos angegeben wurden, als auf dem Schiff vorhanden, benutze die Maximal verfuegbare Anzahl
	 * Suche aus dem Ladungsverzeichnis nach Photonentorpedos und entfene die angeforderte Anzahl aus dieser
	 * Fuege diese dem Schiff hinzu
	 * Ausgabe der nachgeladenen Photonentorpedos auf der Konsole ausgeben
	 */
	public void ladePhotonentorpedos(int anzahlTorpedos) {
		if(this.photonentorpedo <= 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			System.out.println("-=*Click*=-");
		}
 
		if(anzahlTorpedos > this.photonentorpedo) {
			this.photonentorpedo = anzahlTorpedos;
			
			for(Ladung l: this.ladungsVerzeichnis) {
				if(l.getInhalt().equalsIgnoreCase("Photonentorpedos")) {
					l.setMenge(anzahlTorpedos - this.photonentorpedo);
					setPhotonentorpedo(anzahlTorpedos - this.photonentorpedo);
					
					System.out.println(this.schiffsname + " hat [" +  anzahlTorpedos + "] Photonentorpedos eingesetzt");
					System.out.println("");
				}
			}
		}
	}
	
	//10.) Ladungsverzeichnis leeren:
	/**
	 * Suche im Ladungsverzeichnis nach Inhalten mit der Menge Null
	 * Wenn ein Inhalt die Menge Null besitzt, entferne den Inhalt aus dem Ladungsverzeichnis
	 */
	public void raeumeLadungsverzeichnisAuf() {
		for(int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
			if(this.ladungsVerzeichnis.get(i).getMenge() == 0) {
				ladungsVerzeichnis.remove(i);
			}
		}
	}

}
