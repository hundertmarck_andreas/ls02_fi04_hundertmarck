
public class Rechteck {
	
	private double hoehe;
	private double breite;
	
	//Konstruktoren:
	
	public Rechteck() {
		this.hoehe = 0;
		this.breite = 0;
	}
	
	public Rechteck(double hoehe, double breite)
	{
	  setHoehe(hoehe);
	  setBreite(breite);
	}
	
	//Getter/Setter:
	
	public void setHoehe(double hoehe) {
		this.hoehe = hoehe;
	}
	
	public double getHoehe() {
		return this.hoehe;
	}
	
	public void setBreite(double breite) {
		this.breite = breite;
	}
	
	public double getBreite() {
		return this.breite;
	}
	
	//Methoden:
	
	public double getFlaeche() {
		return this.hoehe * this.breite;
	}
	
	public double getUmfang() {
		return 2 * this.hoehe + 2 * this.breite;
	}
	
	public double getDiagonale() {
		return Math.sqrt(Math.pow(this.hoehe, 2) + Math.pow(this.breite, 2));
	}

}
